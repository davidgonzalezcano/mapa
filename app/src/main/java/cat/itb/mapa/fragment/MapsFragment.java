package cat.itb.mapa.fragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import cat.itb.mapa.R;

public class MapsFragment extends Fragment implements OnMapReadyCallback{


    View rootView;
    MapView mapView;
    GoogleMap googleMap;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_maps, container, false);
        //rootView.findViewById(....) etc
        return  rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView = rootView.findViewById(R.id.map); //cargamos la vista
        if (mapView != null) { //llamamos al onCreate etc porque es una vista
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this); //carga el mapa
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);//opcional

        this.googleMap.setMinZoomPreference(15);//del 1 al 21
        this.googleMap.setMinZoomPreference(18);

        //ubicar el mapa
        LatLng itb =  new LatLng(41.4531987,2.1865311);

        MarkerOptions marker = new MarkerOptions();
        marker.position(itb);
        marker.title("Institut tecnologic de barcelona");
        marker.snippet("EL millor institut");
        //marker.icon(BitmapDescriptorFactory.fromResource(android.R.drawable.alert_dark_frame));
        marker.draggable(true);
        this.googleMap.addMarker(marker);

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(itb)
                .zoom(15)
                .bearing(0f)
                .tilt(30)
                .build();

        this.googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        this.googleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                Toast.makeText(getContext(), "LATITUDE: " + latLng.latitude
                                               + " LONGITUDE: " + latLng.longitude, Toast.LENGTH_LONG).show();
                /*Bundle bundle = new Bundle();
                bundle.putDouble("latitude", latLng.latitude);
                bundle.putDouble("longitude", latLng.longitude);
                Fragment1 fragment1 = new Fragment1();
                fragment1.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment1).commit();*/
                /*
                Al fragment1:
                    - bundle = getArguments()
                    -ddouble latitude = bundle.getDouble("latitude");

                 */
            }
        });
    }
}