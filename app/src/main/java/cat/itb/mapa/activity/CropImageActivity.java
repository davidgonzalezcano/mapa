package cat.itb.mapa.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import cat.itb.mapa.R;
import id.zelory.compressor.Compressor;

public class CropImageActivity extends AppCompatActivity {
    private ImageView imageView;
    private Button seleccionarButton, subirButton;
    private ProgressBar cargando;
    DatabaseReference imgref;
    StorageReference storageReference;
    Bitmap thumb_bitmap;
    String nombreImagen;
    byte[] thumb_byte;
    File url;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.crop_image);

        imageView = findViewById(R.id.imageView);
        seleccionarButton = findViewById(R.id.seleccionarButton);
        subirButton = findViewById(R.id.subirButton);

        imgref = FirebaseDatabase.getInstance().getReference().child("mapas");
        storageReference = FirebaseStorage.getInstance().getReference().child("img_comprimidas");

        cargando = new ProgressBar(this);

        seleccionarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.startPickImageActivity(CropImageActivity.this);
            }
        });

        subirButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comprimirImagen();
                subirImagen();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);
            recortarImagen(imageUri);
        }

        if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if(resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                url = new File(resultUri.getPath());
                Picasso.with(this).load(url).into(imageView);
            }
        }
    }

    private void recortarImagen(Uri imageUri) {
        CropImage.activity(imageUri).setGuidelines(CropImageView.Guidelines.ON)
                .setRequestedSize(640, 480)
                .setAspectRatio(2, 1).start(CropImageActivity.this);
    }

    private void comprimirImagen() {
        try {
            thumb_bitmap = new Compressor(CropImageActivity.this)
                    .setMaxHeight(125)
                    .setMaxWidth(125)
                    .setQuality(50)
                    .compressToBitmap(url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        thumb_bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);
        thumb_byte = byteArrayOutputStream.toByteArray();
    }

    public void subirImagen() {
        cargando.setVisibility(View.VISIBLE);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String timestamp = sdf.format(new Date());
        nombreImagen = timestamp + ".jpg";

        final StorageReference ref = storageReference.child(nombreImagen);
        StorageMetadata metadata = new StorageMetadata.Builder()
                .setCustomMetadata("clave1", "valor1")
                .setCustomMetadata("clave2", "valor2")
                .build();
        UploadTask uploadTask = ref.putBytes(thumb_byte, metadata);
        Task<Uri> uriTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if(!task.isSuccessful()) {
                    throw Objects.requireNonNull(task.getException());
                }
                return ref.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                Uri downloadUri = task.getResult();
                imgref.push().child("urlfoto").setValue(downloadUri.toString());
                cargando.setVisibility(View.INVISIBLE);
                Toast.makeText(CropImageActivity.this, "Imagen subida", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
